const pino = require('pino')
const logger = pino({
  prettyPrint: {
    translateTime: true,
    ignore: 'pid,hostname'
  },
  customLevels: {
    myLevel: 32
  },
  level: 'debug'
})

logger.info('hello world')
logger.info(process.env.CI_JOB_URL)
logger.error('hello world')
logger.child({ test: 'test' }).info('hello')
logger.debug('abc')
logger.myLevel('this is a custom level')
logger.fatal('XYYYY')
